package com.smule.suntech.app.ui.musicList

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.smule.suntech.app.R
import com.smule.suntech.app.domain.models.Datum
import kotlinx.android.synthetic.main.item_music_row.view.*


/**
 * Created by Suntech on 9/6/2017.
 */
class MusicListAdapter(contactList: ArrayList<Datum>) : RecyclerView.Adapter<MusicListAdapter.MusicItemViewHolder>() {

    private var mItem: ArrayList<Datum> = contactList

    override fun onBindViewHolder(holder: MusicItemViewHolder?, position: Int) {
        holder?.mName?.text = mItem[position].Name

//        holder?.mImageView?.ima = mItem[position].Name

        Glide.with(holder?.itemView?.context)
                .load(mItem[position].PictureToken)
                .into(holder?.mImageView)


    }

    override fun getItemCount(): Int = mItem.count()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MusicItemViewHolder {
        val itemView = LayoutInflater.from(parent?.context).inflate(R.layout.item_music_row, parent, false)
        return MusicItemViewHolder(itemView)
    }

    class MusicItemViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        val mName: TextView by lazy { v.txtName }
        val mImageView: ImageView by lazy { v.music_list_iv }

    }
}