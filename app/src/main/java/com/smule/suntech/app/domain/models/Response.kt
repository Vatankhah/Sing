package com.smule.suntech.app.domain.models

import com.google.gson.annotations.SerializedName
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * Created by Suntech on 9/19/2017.
 */


open class BaseResponse(@SerializedName("Status") var  Status: String? = "",@SerializedName("Message") var Message: String? = "")

open class RegisterResponse() : BaseResponse()
open class LoginResponse() : BaseResponse()
data class MusicListResponce(@SerializedName("Result") var Result: MusicData) :BaseResponse()
data class MusicData(@SerializedName("Data")var Data: ArrayList<Datum>,
                     @SerializedName("ServerDateTime")var ServerDateTime: String? = "",
                     @SerializedName("PageNumber")var PageNumber: String? = "",
                     @SerializedName("PageSize")var PageSize: String? = "",
                     @SerializedName("From")var From: String? = "",
                     @SerializedName("To")var To: String? = "",
                     @SerializedName("TotalCount")var TotalCount: String? = "",
                     @SerializedName("HasData")var HasData: String? = "")

data class ProfileResponse(@SerializedName("Result")var Result: ProfileData) : BaseResponse()
//open class ProfileData(
//                       @SerializedName("Username")var Username: String? = "",
//                       @SerializedName("FirstName")var FirstName: String? = "",
//                       @SerializedName("LastName")var LastName: String? = "",
//                       @SerializedName("PictureToken")var PictureToken: String? = "")
//                      // @SerializedName("PictureToken")var PictureToken: MultipartBody.Part? = null)


open class ProfileData(
        @SerializedName("Username") var Username: RequestBody? = null,
        @SerializedName("FirstName") var FirstName: RequestBody? = null,
        @SerializedName("LastName") var LastName: RequestBody? = null,
        @SerializedName("PictureToken") var PictureToken: RequestBody? = null)

//open class ProfileData(
//        @SerializedName("partMap") var partMap: Map<String, RequestBody>,
//        @SerializedName("file") var file: MultipartBody.Part)
{
     @SerializedName("CellPhoneNumber")var CellPhoneNumber: String? = ""
     @SerializedName("Id")var Id: Int = 0
}

open class VideoResponse() : BaseResponse()

