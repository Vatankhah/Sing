package com.smule.suntech.app.injector.module;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mohammadrezanajafi on 2/28/16.
 */

@Module
public class FragmentModule {

    final Fragment mFragment;

    public FragmentModule(Fragment fragment) {
        mFragment = fragment;
    }

    @Provides
    public Context context(){ return mFragment.getContext();}

    @Provides
    public FragmentManager provideFragmentManager(){ return mFragment.getFragmentManager();}
}
