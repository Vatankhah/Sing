package com.smule.suntech.app.network.rest

import android.support.v4.util.LruCache
import com.smule.suntech.app.domain.models.*
import com.smule.suntech.app.network.repository.Repository
import retrofit2.Retrofit
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject


/**
 * Created by za_khodabakhshi on 3/2/16.
 */
class RestDataSource
@Inject
constructor(retrofit: Retrofit) : Repository {


    private val mAppApi: AppApi

    init {
        mAppApi = retrofit.create(AppApi::class.java)
    }

    private val mApiObservablesCache = LruCache<Class<*>, Observable<*>>(10)

    /**
     * Method to either return a cached observable or prepare a new one.

     * @param unPreparedObservable
     * *
     * @param clazz
     * *
     * @param cacheObservable
     * *
     * @param useCache
     * *
     * @return Observable ready to be subscribed to
     */
    override fun getPreparedObservable(unPreparedObservable: Observable<*>, clazz: Class<*>,
                                       cacheObservable: Boolean, useCache: Boolean): Observable<*> {

        var preparedObservable: Observable<*>? = null

        if (useCache)
        //this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = mApiObservablesCache.get(clazz)

        if (preparedObservable != null) return preparedObservable

        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread()).observeOn(
                AndroidSchedulers.mainThread())

        if (cacheObservable) {
            preparedObservable = preparedObservable!!.cache()
            mApiObservablesCache.put(clazz, preparedObservable!!)
        }


        return preparedObservable
    }

    /**
     * Method to clear the entire cache of observables
     */
    override fun clearCache() {
        mApiObservablesCache.evictAll()
    }

    override fun getLoginResponse(mLoginModel: LoginModel): Observable<LoginResponse> =
            mAppApi.getLoginResponse(mLoginModel.cellPhoneNumber, mLoginModel.verificationCode)

    override fun getRegisterResponse(mRegisterModel: RegisterModel): Observable<RegisterResponse> =
            mAppApi.getRegisterResponse(mRegisterModel.mPhoneNumber)

    override fun getMusicListResponse(): Observable<MusicListResponce> = mAppApi.getMusicListResponse()

    override fun getLogout(): Observable<BaseResponse> = mAppApi.getLogout()

    override fun getProfile(): Observable<ProfileResponse> = mAppApi.getProfile()

    override fun saveProfile(mProfileData: ProfileData): Observable<BaseResponse> =
            mAppApi.saveProfile(mProfileData.FirstName , mProfileData.LastName , mProfileData.Username , mProfileData.PictureToken)
}
