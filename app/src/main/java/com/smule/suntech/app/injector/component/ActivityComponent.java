package com.smule.suntech.app.injector.component;

import com.smule.suntech.app.injector.module.ActivityModule;
import com.smule.suntech.app.injector.module.FragmentModule;
import com.smule.suntech.app.injector.scopes.ActivityScope;
import com.smule.suntech.app.ui.activation.ActivationActivity;
import com.smule.suntech.app.ui.main.MainActivity;
import com.smule.suntech.app.ui.profile.ProfileActivity;
import com.smule.suntech.app.ui.register.RegisterActivity;

import dagger.Subcomponent;

/**
 * Created by za_khodabakhshi on 2/28/16.
 */

@ActivityScope
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(ActivationActivity loginActivity);
    void inject(ProfileActivity profileActivity);
    void inject(RegisterActivity registerActivity);
    void inject(MainActivity mainActivity);

    FragmentComponent plus(FragmentModule module);
}
