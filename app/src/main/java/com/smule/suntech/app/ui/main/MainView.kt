package com.smule.suntech.app.ui.main

import com.smule.suntech.app.ui.base.BaseView


/**
 * Created by Suntech on 9/19/2017.
 */
interface MainView : BaseView {
    fun onSuccessLogout()
    fun onFailLogout()
}