package com.smule.suntech.app.domain.interactor

import com.smule.suntech.app.network.repository.Repository
import com.smule.suntech.app.domain.interactor.base.BaseUseCaseWithoutParam
import com.smule.suntech.app.domain.models.MusicListResponce
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject


/**
 * Created by Suntech on 9/10/2017.
 */

class GetMusicListResponse @Inject constructor(private val mRepository: Repository)  : BaseUseCaseWithoutParam<MusicListResponce> {

    override fun execute(): Observable<MusicListResponce> {
            return mRepository.getMusicListResponse()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
    }
}