package com.smule.suntech.app.ui.base

/**
 * Created by Suntech on 9/3/2017.
 */
interface BasePresenter2 {

    fun onStart()
    fun onStop()
    fun onPause()

    fun attachView(view: BaseView)
    fun detachView()

    fun onRefreshData(){
        onDataLoad(true)
    }
    fun onCreate(){
        onDataLoad(false)
    }

    fun onDataLoad(isRefresh: Boolean)

}