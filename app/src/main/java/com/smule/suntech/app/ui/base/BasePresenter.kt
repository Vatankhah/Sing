package com.hamrah.sun.sunpayment.ui.base

import rx.Subscription
import rx.subscriptions.CompositeSubscription

/**
 * Created by Suntech on 9/19/2017.
 */

public open class BasePresenter<V> : AbstractPresenter<V>() {

    protected var isFirstTime = true
        private set
    private val compositeSubscription = CompositeSubscription()

    override fun onViewAttached(view: V) {
        super.onViewAttached(view)
        if (isFirstTime) {
            isFirstTime = false
            onViewAttachedForFirstTime(view)
        }
    }

    protected fun addSubscription(subscription: Subscription) {
        compositeSubscription.add(subscription)
    }

    open fun onViewAttachedForFirstTime(view: V) {
    }

    override fun onViewDetached() {
        super.onViewDetached()
        if (compositeSubscription != null)
            compositeSubscription.unsubscribe()

    }
}
