//package com.smule.suntech.app.domain.interactor.base
//
//import com.smule.suntech.app.domain.executer.UseCaseExecutor
//import com.smule.suntech.app.domain.executer.PostExecutionThread
//import rx.Observable
//
///**
// * Created by Suntech on 9/18/2017.
// */
//abstract class ObservableUseCase<R, Q> : Usecase<rx.Observable<*> , Q> {
//
//    val schedulersTransformer: Observable.Transformer<in Q, out R>
//
//   public constructor(useCaseExecutor: UseCaseExecutor,
//                postExecutionThread: PostExecutionThread):super(useCaseExecutor, postExecutionThread) {
//        schedulersTransformer = { rObservable ->
//            rObservable.subscribeOn(useCaseExecutor.getScheduler())
//                    .observeOn(postExecutionThread.getScheduler())
//        }
//    }
//
//    override fun execute(param: Q): rx.Observable<R> {
//        return rx.Observable.create<Any> { subscriber ->
//            interact(param).compose(schedulersTransformer).subscribe({ o ->
//                subscriber.onNext(o)
//                subscriber.onCompleted()
//            }) { error ->
//                //changing error message
//                if (error.message.toLowerCase().contains("timeout") || error.message
//                        .toLowerCase().contains("unable to resolve")
//                        || error.message
//                        .toLowerCase().contains("failed to connect to"))
//                    subscriber.onError(Throwable("خطا:اشکال ارتباط با سرور"))
//                else
//                    subscriber.onError(error)
//            }
//        }.compose(schedulersTransformer as rx.Observable.Transformer<in Any, out R>)
//    }
//}
//
