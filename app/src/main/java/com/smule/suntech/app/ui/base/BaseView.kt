package com.smule.suntech.app.ui.base

import android.support.annotation.StringRes

/**
 * Created by Suntech on 9/3/2017.
 */
interface BaseView {
    fun onLoadStarted()
    fun onLoadFinished()

    fun onLoadFailed(@StringRes msg: Int)
    fun onLoadFailed(msg: String)

}