package com.hamrah.sun.sunpayment.ui.base

import java.lang.ref.WeakReference


open class AbstractPresenter<V> : Presenter<V> {
    private var view: WeakReference<V>? = null

    override fun onViewAttached(view: V) {
        this.view = WeakReference(view)
    }

    override fun onViewDetached() {
        if (view != null)
            view!!.clear()
    }

    override fun onDestroyed() {
        if (view != null)
            view!!.clear()
        view = null
    }

    fun getView(): V? {
        return if (view != null)
            view!!.get()
        else
            null
    }

    override val isViewAttached: Boolean
        get() = view != null && view!!.get() != null


}
