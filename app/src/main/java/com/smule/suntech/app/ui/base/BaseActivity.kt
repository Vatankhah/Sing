package com.hamrah.sun.sunpayment.ui.base

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.afollestad.materialdialogs.GravityEnum
import com.afollestad.materialdialogs.MaterialDialog
import com.smule.suntech.app.R
import com.smule.suntech.app.SingApp
import com.smule.suntech.app.injector.component.ActivityComponent
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import javax.inject.Inject

/**
 * Created by S.Vatankhah on 9/19/2017.
 */

public abstract class BaseActivity<P : BasePresenter<V>, V> : AppCompatActivity() {


    @Inject lateinit var mPresenter: P

    protected abstract fun injectDependencies(component: ActivityComponent)

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies(SingApp.getComponent(this))
        super.onCreate(savedInstanceState)

    }

    override fun onResume() {
        super.onResume()
        mPresenter.onViewAttached(this as V)
    }

    override fun onPause() {
        mPresenter.onViewDetached()
        super.onPause()
    }

    override fun onDestroy() {
        mPresenter.onDestroyed()
        super.onDestroy()
    }

    protected fun showSnack(message: String, snakButtonListener: View.OnClickListener, textResId: Int) {
        val snackbar = Snackbar
                .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.RED)
                .setAction(textResId, snakButtonListener)
        snackbar.show()
    }

    protected fun showSnack(message: String) {
        val snackbar = Snackbar
                .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.RED)
        snackbar.show()
    }


    protected val mProgressDialog: MaterialDialog by lazy {
        MaterialDialog.Builder(this)
                .content(R.string.please_wait)
                .progress(true, 0)
                .contentGravity(GravityEnum.CENTER)
                .cancelable(false)
                .autoDismiss(false)
                .build()
    }

    fun getDialog(titleResId: Int): MaterialDialog {


        var mMaterialDialog: MaterialDialog = MaterialDialog.Builder(this)
                .content(titleResId)
                .progress(true, 0)
                .contentGravity(GravityEnum.CENTER)
                .cancelable(false)
                .autoDismiss(false)
                .build()

        mMaterialDialog.show()
        return mMaterialDialog
    }

    fun loadFragment(fragment: Fragment) {
        loadFragment(fragment, true)
    }

    fun loadFragment(fragment: Fragment, addToBackStack: Boolean?) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.main_frame_container, fragment)
        if (addToBackStack!!) {1
            fragmentTransaction.addToBackStack(fragment.javaClass.simpleName)
        }
        fragmentTransaction.commit()
    }

    fun getCurrentFragment(): Fragment? {
        val fragmentManager = this.supportFragmentManager
        val fragments = fragmentManager.fragments
        if (fragments != null) {
            for (fragment in fragments) {
                if (fragment != null && fragment.isVisible)
                    return fragment
            }
        }
        return null
    }

}