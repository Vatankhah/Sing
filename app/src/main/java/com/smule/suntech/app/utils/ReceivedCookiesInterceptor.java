package com.smule.suntech.app.utils;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Request;

/**
 * Created by Suntech on 9/11/2017.
 */

public class ReceivedCookiesInterceptor  implements Interceptor {
    public static final String PREF_COOKIES = "PREF_COOKIES";
    private Context context;

    public ReceivedCookiesInterceptor(Context context){
        this.context = context;
    }

    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        okhttp3.Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            HashSet<String> cookies = new HashSet<>();

            for (String header : originalResponse.headers("Set-Cookie")) {
                cookies.add(header);
            }
            PreferenceManager.getDefaultSharedPreferences(context).edit()
                    .putStringSet(PREF_COOKIES, cookies)
                    .apply();
        }

        return originalResponse;
    }
}


