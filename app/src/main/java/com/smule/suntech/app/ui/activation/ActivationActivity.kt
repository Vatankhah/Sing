package  com.smule.suntech.app.ui.activation

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.view.animation.Animation

import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.hamrah.sun.sunpayment.ui.base.BaseActivity
import com.smule.suntech.app.R
import com.smule.suntech.app.domain.models.LoginModel
import com.smule.suntech.app.injector.component.ActivityComponent
import com.smule.suntech.app.ui.main.MainActivity
import com.smule.suntech.app.ui.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_activation.*
import android.view.animation.LinearInterpolator
import android.view.animation.TranslateAnimation
import com.smule.suntech.app.ui.profile.ProfileActivity


/**
 * Created by S.Vatankhah on 9/19/2017.
 */

class ActivationActivity : BaseActivity<ActivationPresenter, ActivationView>(), ActivationView
{
    override fun injectDependencies(component: ActivityComponent) {
        component.inject(this);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activation)

        setImageLoop()

        onClicks()
    }



    private fun setImageLoop() {

        imgRightCloud.post(Runnable {
            var mfloat : Float =container.getWidth().toFloat()
            val outAnim = TranslateAnimation(0f, mfloat, 0f, 0f)
            // move from 0 (START) to width (PARENT_SIZE)
            outAnim.interpolator = LinearInterpolator()
            outAnim.repeatMode = Animation.INFINITE // repeat the animation
            outAnim.repeatCount = Animation.INFINITE
            outAnim.duration = 20000

            val inAnim = TranslateAnimation(mfloat , 0f, 0f, 0f)
            // move from out width (-PARENT_SIZE) to 0 (START)
            inAnim.interpolator = LinearInterpolator()
            inAnim.repeatMode = Animation.INFINITE
            inAnim.repeatCount = Animation.INFINITE
            inAnim.duration = 20000 // same duration as the first

            imgRightCloud.startAnimation(outAnim) // start first anim
            imgLeftCloud.startAnimation(inAnim) // start second anim
        })
    }

    private fun onClicks() {
        val username: String = PreferenceManager.getDefaultSharedPreferences(this).getString("username", "")
        lblPhoneNumber.setText(username)
        btnActivation.setOnClickListener {
            val mActivationCode: String = txtActivationCode.text.toString()
            if (mActivationCode.isNullOrEmpty()) {
                txtActivationCode.requestFocus()
                txtActivationCode.error = getString(R.string.field_required)
            } else {
                mPresenter.activation(LoginModel(username, mActivationCode))
            }
        }

        btnTryAgain.setOnClickListener {
            intent = Intent(this@ActivationActivity, RegisterActivity::class.java)
            intent.putExtra("type","mobile")
            startActivity(intent)
        }
    }


    override fun onLoadStarted() {
        loginProgressBar.visibility = View.VISIBLE
        btnActivation.setText("")
    }

    override fun onLoadFinished() {
    }

    override fun onLoadFailed(msg: Int) {
    }

    override fun onLoadFailed(msg: String) {
        MaterialDialog.Builder(this)
                .title(msg)
                .positiveText(getString(R.string.ok))
                .onPositive { materialDialog, _ -> materialDialog.dismiss() }
                .show()
    }

    override fun onSuccessLogin() {
        var intent = Intent(this@ActivationActivity, ProfileActivity::class.java)
        startActivity(intent)
        finish()

    }

    override fun onLoginError(msg: String?) {
        MaterialDialog.Builder(this)
                .title(msg ?: getString(R.string.try_again))
                .positiveText(getString(R.string.ok))
                .onPositive { materialDialog, _ -> materialDialog.dismiss() }
                .show()
    }



}