package com.smule.suntech.app.network.rest

import com.smule.suntech.app.domain.models.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import rx.Observable


/**
 * Created by Suntech on 9/4/2017.
 */
interface AppApi {

    companion object {
        val END_POINT = "http://service.sing.sunsct.com/"
    }

    @POST("Account/login/")
    @Headers(
            "Accept: application/json"
    )
    fun getLoginResponse(@Query("cellPhoneNumber") cellPhoneNumber: String?,
                         @Query("verificationCode") verificationCode: String?,
                         @Query("channel") channel: Int = 0): Observable<LoginResponse>


    @POST("Account/Register/")
    fun getRegisterResponse(@Query("cellPhoneNumber") cellPhoneNumber: String?,
                            @Query("channel") channel: Int = 0): Observable<RegisterResponse>

    @GET("EndUser/Audio/List")
    @Headers("Accept: application/json")
    fun getMusicListResponse(): Observable<MusicListResponce>


    @GET("EndUser/video/List")
    @Headers("Accept: application/json")
    fun getVideoListResponse(): Observable<MusicListResponce>

    @POST("Account/logout/")
    @Headers("Accept: application/json")
    fun getLogout(): Observable<BaseResponse>

    @GET("Account/Profile/")
    @Headers("Accept: application/json")
    fun getProfile(): Observable<ProfileResponse>


//    @Multipart
//    @POST("EndUser/Account/SaveProfile")
//    fun saveProfile(@PartMap() partMap: Map<String, RequestBody>,
//                    @Part file: MultipartBody.Part): Observable<BaseResponse>

    @Multipart
    @POST("EndUser/Account/SaveProfile")
    fun saveProfile(@Part("firstName") firstName: RequestBody?,
                    @Part("lastName") lastName: RequestBody?,
                    @Part("username") username: RequestBody?,
                    @Part("file\"; filename=\"pp.png\" ") pictureToken:  RequestBody?): Observable<BaseResponse>


//    //@FormUrlEncoded
//    @POST("EndUser/Account/SaveProfile")
//    @Headers("Accept: application/json")
//    fun saveProfile(@Query("firstName") firstName: String?,
//                    @Query("lastName") lastName: String?,
//                    @Query("username") username: String?,
//                    @Query("PictureToken") pictureToken: String?): Observable<BaseResponse>


//    @POST("video/Create/")
//    @Headers("Accept: application/json")
//    fun videoCreate(): Observable<VideoResponse>

//    http://service.sing.sunsct.com/EndUser/Audio/search?keyValue=شجریان
//    http://service.sing.sunsct.com/EndUser/Audio/get?audioId=1
//    http://service.sing.sunsct.com/EndUser/verse/list?audioId=1


}
