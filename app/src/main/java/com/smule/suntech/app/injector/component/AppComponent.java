package com.smule.suntech.app.injector.component;

import com.smule.suntech.app.injector.module.ActivityModule;
import com.smule.suntech.app.injector.module.BaseAppModule;

import dagger.Component;

import javax.inject.Singleton;

/**
 * Created by za_khodabakhshi on 2/28/16.
 */

@Singleton
@Component(modules = {BaseAppModule.class})
public interface AppComponent {

    ActivityComponent plus(ActivityModule module);
}
