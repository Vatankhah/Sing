package com.smule.suntech.app.ui.musicList

import com.hamrah.sun.sunpayment.ui.base.BasePresenter
import com.smule.suntech.app.R
import com.smule.suntech.app.domain.interactor.GetMusicListResponse
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Suntech on 9/10/2017.
 */
class MusicListPresenter @Inject constructor(private val mGetMusicListResponse: GetMusicListResponse) : BasePresenter<MusicListItemView>() {


    override fun onViewAttached(view: MusicListItemView) {
        super.onViewAttached(view)

        getView()?.onLoadStarted()
        addSubscription(mGetMusicListResponse.execute().subscribe({ it ->
            getView()?.onLoadFinished()
            Timber.d("mGetMusicListResponse it is [%s]", it)
            if (it.Status.equals("success")) {
                getView()?.bindMusicListResponse(it)
            } else {
                getView()?.onLoadFailed(it.Message ?: "try again")
            }
        }, {
            getView()?.onLoadFinished()
            getView()?.onLoadFailed(R.string.try_again)
        }))
    }

}