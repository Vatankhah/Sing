package com.smule.suntech.app.ui.register

import com.smule.suntech.app.ui.base.BaseView

/**
 * Created by Suntech on 9/5/2017.
 */
interface RegisterView : BaseView {
    fun onSuccessRegister()
    fun onFailRegister(msg: String?)
}