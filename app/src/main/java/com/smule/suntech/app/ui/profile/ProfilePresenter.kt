package com.smule.suntech.app.ui.profile

import com.hamrah.sun.sunpayment.ui.base.BasePresenter
import com.smule.suntech.app.R
import com.smule.suntech.app.domain.interactor.GetProfileUseCase
import com.smule.suntech.app.domain.models.BaseResponse
import com.smule.suntech.app.domain.models.ProfileData
import javax.inject.Inject

/**
 * Created by S.Vatankhah on 9/26/2017.
 */
class ProfilePresenter @Inject constructor(var mGetProfileUseCase: GetProfileUseCase) : BasePresenter<ProfileView>() {


    fun saveProfile(mProfileData: ProfileData) {
        getView()?.onLoadStarted();
        addSubscription(mGetProfileUseCase.execute(mProfileData).subscribe({ response: BaseResponse? ->
            getView()?.onLoadFinished();
            if (response!!.Status.equals("success"))
                getView()?.onSuccessSaveProfile()
            else
                getView()?.onErrorSaveProfile(response.Message)
        }, {
            getView()?.onLoadFinished()
            getView()?.onLoadFailed(R.string.try_again)
        }))


    }



}