package com.smule.suntech.app.domain.interactor.base

import rx.Observable

/**
 * Created by Suntech on 9/4/2017.
 */
interface BaseUsecase<T, U> {
    fun execute(param : U): Observable<T>
}

interface BaseUseCaseWithoutParam<T> {
    fun execute(): Observable<T>
}