package com.hamrah.sun.sunpayment.ui.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smule.suntech.app.SingApp
import com.smule.suntech.app.injector.component.FragmentComponent
import javax.inject.Inject

/**
 * Created by S.Vatankhah on 9/19/2017.
 */

abstract class BaseFragment<P : BasePresenter<V>, V> : Fragment() {

    @Inject lateinit var mPresenter: P;


    @get:LayoutRes
    abstract val layout: Int

    protected abstract fun injectDependencies(component: FragmentComponent)
    protected abstract fun onCreateCompleted()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        //return super.onCreateView(inflater, container, savedInstanceState)
        return inflater!!.inflate(layout, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        injectDependencies(SingApp.getComponent(this))
        super.onViewCreated(view, savedInstanceState)
        onCreateCompleted()
    }

    override fun onResume() {
        super.onResume()
        mPresenter.onViewAttached(this as V)
    }

    override fun onPause() {
        mPresenter.onViewDetached()
        super.onPause()
    }

    override fun onDestroyView() {
        mPresenter.onDestroyed()
        super.onDestroyView()
    }
}