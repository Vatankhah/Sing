package com.smule.suntech.app.ui.main

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Gravity
import android.widget.Toast
import com.hamrah.sun.sunpayment.ui.base.BaseActivity
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.Nameable
import com.smule.suntech.app.R
import com.smule.suntech.app.injector.component.ActivityComponent
import com.smule.suntech.app.ui.base.BaseActivity2
import com.smule.suntech.app.ui.musicList.MusicListFragment
import com.smule.suntech.app.ui.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainPresenter, MainView>() , MainView{

    override fun injectDependencies(component: ActivityComponent) {
       component.inject(this)
    }


    private var result: Drawer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        initDrawerBuilder(savedInstanceState)
        loadFragment(MusicListFragment())


    }

    private fun initDrawerBuilder(savedInstanceState: Bundle?) {
        val headerResult = AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.drawer_background)
                .addProfiles(
                        ProfileDrawerItem().withName("xxx").withEmail("xxx@gmail.com").withIcon(resources.getDrawable(R.mipmap.ic_launcher))
                )
                .withOnAccountHeaderListener({ _, _, _ ->
                    startActivity(Intent(this@MainActivity, BaseActivity2::class.java))
                    false
                })
                .build()

//        result = DrawerBuilder()
//                .withActivity(this)
//                .withAccountHeader(headerResult)
//                .withDrawerGravity(Gravity.RIGHT)
//                //.withSavedInstance(savedInstanceState)
//                .withDisplayBelowStatusBar(false)
//                .withTranslucentStatusBar(false)
//                .withDrawerLayout(R.layout.material_drawer_fits_not)
//                .addDrawerItems(
//                        PrimaryDrawerItem().withName("صفحه اصلی ").withIcon(R.drawable.sing_logo),
//                        SecondaryDrawerItem().withName("لیست موزیک").withIcon(R.drawable.sing_logo)
//                )
//                .withOnDrawerItemClickListener { _, _, drawerItem ->
//                    if (drawerItem is Nameable<*>) {
//                        Toast.makeText(this@MainActivity, (drawerItem as Nameable<*>).name.getText(this@MainActivity), Toast.LENGTH_SHORT).show()
//                    }
//
//                    false
//                }.build()

//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        supportActionBar?.setHomeButtonEnabled(false)


        result = DrawerBuilder()
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withActivity(this)
                .withDrawerGravity(Gravity.RIGHT)
                .withSavedInstance(savedInstanceState)
                .withDisplayBelowStatusBar(false)
                .withTranslucentStatusBar(false)
                .withDrawerLayout(R.layout.material_drawer_fits_not)
                .addDrawerItems(
                        PrimaryDrawerItem().withName(R.string.drawer_item_home).withIcon(R.drawable.sing_logo),
                        SecondaryDrawerItem().withName(R.string.drawer_item_logout).withIcon(R.drawable.logout)
                )
                .withOnDrawerItemClickListener { view, position, drawerItem ->
                    if (drawerItem is Nameable<*>) {
                        Toast.makeText(this@MainActivity, (drawerItem as Nameable<*>).name.getText(this@MainActivity), Toast.LENGTH_SHORT).show()
                        if (drawerItem is SecondaryDrawerItem)
                            mPresenter.logout()
                    }

                    false
                }.build()

        //  supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        // supportActionBar!!.setHomeButtonEnabled(true)
    }


    override fun onLoadStarted() {
    }

    override fun onSuccessLogout() {
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString("username", "")
                .apply()
        intent = Intent(MainActivity@this, RegisterActivity::class.java)
        startActivity(intent)

    }

    override fun onLoadFinished() {
    }

    override fun onFailLogout() {
    }

    override fun onLoadFailed(msg: Int) {
    }

    override fun onLoadFailed(msg: String) {
    }
}