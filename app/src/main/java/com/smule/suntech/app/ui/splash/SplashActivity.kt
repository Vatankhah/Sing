package com.smule.suntech.app.ui.splash

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import com.smule.suntech.app.ui.main.MainActivity
import com.smule.suntech.app.ui.register.RegisterActivity

/**
 * Created by Suntech on 9/20/2017.
 */

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val username: String = PreferenceManager.getDefaultSharedPreferences(this).getString("username", "")
        if (!username.isNullOrEmpty()) {
            intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        else {
            intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}