package com.smule.suntech.app.injector.module

import javax.inject.Singleton
import android.app.Application
import android.content.Context
import android.preference.PreferenceManager
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.smule.suntech.app.network.repository.Repository
import com.smule.suntech.app.network.rest.AppApi
import com.smule.suntech.app.network.rest.RestDataSource
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.subscriptions.CompositeSubscription
import com.smule.suntech.app.utils.AddCookiesInterceptor
import com.smule.suntech.app.utils.ReceivedCookiesInterceptor


/**
 * Created by Suntech on 9/3/2017.
 */

@Module
class BaseAppModule constructor(private val mContext: Context) {

    @Provides
    fun provideCompositeSubscription(): CompositeSubscription {
        return CompositeSubscription()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpCache(application: Context): okhttp3.Cache {
        val cacheSize = 10 * 1024 * 1024
        return okhttp3.Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    fun provideContext(): Context {
        return mContext
    }

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application)
    }


    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(AppApi.END_POINT)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    internal fun provideDataRepository(restDataSource: RestDataSource): Repository {
        return restDataSource
    }


    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder()
                .create()
    }

//    @Provides
//    @Singleton
//    internal fun provideCookieJAr(): PersistentCookieJar =
//            PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(mContext))


    @Provides
    @Singleton
    internal fun provideOkHttpClient(cache: okhttp3.Cache): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(AddCookiesInterceptor(mContext))
                .addInterceptor(ReceivedCookiesInterceptor(mContext))
                .cache(cache)
                .build()
    }
}