package com.smule.suntech.app.ui.profile

import com.smule.suntech.app.ui.base.BaseView

/**
 * Created by Suntech on 9/26/2017.
 */
interface ProfileView : BaseView {

  fun  onSuccessSaveProfile()
  fun  onErrorSaveProfile(mString : String?)

}