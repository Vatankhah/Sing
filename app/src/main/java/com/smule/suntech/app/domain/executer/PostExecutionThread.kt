package com.smule.suntech.app.domain.executer

import rx.Scheduler

/**
 * Created by Suntech on 9/18/2017.
 */
interface PostExecutionThread
{
    fun getScheduler() : Scheduler
    //


}