package com.smule.suntech.app.ui.musicList

 import com.smule.suntech.app.domain.models.MusicListResponce
 import com.smule.suntech.app.ui.base.BaseView

/**
 * Created by Suntech on 9/10/2017.
 */
interface MusicListItemView : BaseView {

    fun bindMusicListResponse(mMusicListResponse: MusicListResponce)

    fun unSuccessGetItem()

}