package com.hamrah.sun.sunpayment.ui.base


interface Presenter<V> {

    /**
     * Called when the view attached to the screen.
     *
     * @param view the view that the presenter interacts with
     */
    fun onViewAttached(view: V)

    /**
     * Called when the view detached from the screen.
     */
    fun onViewDetached()

    /**
     * Called when a user leaves the view completely.
     */
    fun onDestroyed()

    /**
     * @return Returns true if the view is currently attached to the presenter, otherwise returns false.
     */
    val isViewAttached: Boolean
}
