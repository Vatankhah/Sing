package com.smule.suntech.app

import android.app.Application
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.smule.suntech.app.injector.component.ActivityComponent
import com.smule.suntech.app.injector.component.AppComponent
import com.smule.suntech.app.injector.component.DaggerAppComponent
import com.smule.suntech.app.injector.component.FragmentComponent
import com.smule.suntech.app.injector.module.ActivityModule
import com.smule.suntech.app.injector.module.BaseAppModule
import com.smule.suntech.app.injector.module.FragmentModule
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import kotlin.properties.Delegates

/**
 * Created by Suntech on 9/4/2017.
 */
class SingApp : Application() {
    var component: AppComponent by Delegates.notNull<AppComponent>()

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder().baseAppModule(BaseAppModule(this)).build()

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/sans.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())
    }

    companion object {
        fun getComponent(fragment: Fragment): FragmentComponent {
            assert(fragment.activity != null)
            val activity = fragment.activity
            val application = fragment.context.applicationContext as SingApp
            return application.component.plus(ActivityModule(activity)).plus(FragmentModule(fragment))
        }

        fun getComponent(activity: AppCompatActivity): ActivityComponent {
            val application = activity.applicationContext as SingApp
            return application.component.plus(ActivityModule(activity))
        }

    }


}