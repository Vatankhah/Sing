package com.smule.suntech.app.injector.module;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import dagger.Module;
import dagger.Provides;

/**
 * Created by za_khodabakhshi on 2/28/16.
 */

@Module
public class ActivityModule {

    final Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    public Activity activity(){ return mActivity;}

    @Provides
    public Context context(){ return activity();}

    @Provides
    LayoutInflater layoutInflater(){ return mActivity.getLayoutInflater();}
}