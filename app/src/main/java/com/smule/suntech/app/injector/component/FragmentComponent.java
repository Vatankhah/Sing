package com.smule.suntech.app.injector.component;

import com.smule.suntech.app.injector.module.FragmentModule;
import com.smule.suntech.app.injector.scopes.FragmentScope;
import com.smule.suntech.app.ui.musicList.MusicListFragment;
import dagger.Subcomponent;

/**
 * Created by za_khodabakhshi on 2/28/16.
 */


@FragmentScope
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {

    void inject(MusicListFragment musicListFragment);
}
