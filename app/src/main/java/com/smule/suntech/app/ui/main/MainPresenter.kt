package com.smule.suntech.app.ui.main

import com.hamrah.sun.sunpayment.ui.base.BasePresenter
import com.smule.suntech.app.R
import com.smule.suntech.app.domain.interactor.GetLogoutUseCase
import javax.inject.Inject

/**
 * Created by S.Vatankhah on 9/19/2017.
 */

public class MainPresenter @Inject constructor(var mGetLogoutUseCase: GetLogoutUseCase) : BasePresenter<MainView>() {
    fun logout() {
        getView()?.onLoadStarted()

        addSubscription(mGetLogoutUseCase.execute().subscribe({ response ->
            getView()?.onLoadFinished()

            if (response.Status.equals("success"))
              getView()?.onSuccessLogout()
            else {
                getView()?.onFailLogout()
            }
        }, {
            getView()?.onLoadFinished()
            getView()?.onLoadFailed(R.string.try_again)
        }))
    }
}


