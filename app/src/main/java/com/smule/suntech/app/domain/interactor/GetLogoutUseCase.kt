package com.smule.suntech.app.domain.interactor

import com.smule.suntech.app.network.repository.Repository
import com.smule.suntech.app.domain.interactor.base.BaseUseCaseWithoutParam
import com.smule.suntech.app.domain.models.BaseResponse
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Suntech on 9/20/2017.
 */

class GetLogoutUseCase @Inject constructor(var mRepository: Repository) : BaseUseCaseWithoutParam<BaseResponse> {
    override fun execute(): Observable<BaseResponse> {
        return mRepository.getLogout().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}