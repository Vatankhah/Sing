package com.smule.suntech.app.ui.activation

import com.hamrah.sun.sunpayment.ui.base.BasePresenter
import com.smule.suntech.app.R
import com.smule.suntech.app.domain.interactor.GetLoginUseCase
import com.smule.suntech.app.domain.models.LoginModel
import com.smule.suntech.app.domain.models.LoginResponse
import javax.inject.Inject

/**
 * Created by Suntech on 9/4/2017.
 */
class ActivationPresenter @Inject constructor(val mGetLoginUseCase: GetLoginUseCase) : BasePresenter<ActivationView>() {

    fun activation(mLoginModel: LoginModel) {
        getView()?.onLoadStarted();

        addSubscription(mGetLoginUseCase.execute(mLoginModel).subscribe({ response: LoginResponse? ->
            getView()?.onLoadFinished();
            if (response!!.Status.equals("success"))
                getView()?.onSuccessLogin()
            else
                getView()?.onLoginError(response.Message)
        }, {
            getView()?.onLoadFinished()
            getView()?.onLoadFailed(R.string.try_again)
        }))
    }
}