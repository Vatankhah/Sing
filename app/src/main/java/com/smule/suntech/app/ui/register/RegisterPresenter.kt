package com.smule.suntech.app.ui.register

import com.hamrah.sun.sunpayment.ui.base.BasePresenter
import com.smule.suntech.app.R
import com.smule.suntech.app.domain.interactor.GetRegisterUseCase
import com.smule.suntech.app.domain.models.RegisterModel
import javax.inject.Inject

/**
 * Created by Suntech on 9/5/2017.
 */
class RegisterPresenter @Inject constructor(var mRegisterUseCase: GetRegisterUseCase) : BasePresenter<RegisterView>() {

    fun register(mRegisterModel: RegisterModel) {
        getView()?.onLoadStarted()

        addSubscription(mRegisterUseCase.execute(mRegisterModel).subscribe({ response ->
            getView()?.onLoadFinished()
            if (response.Status.equals("success")) {
                getView()?.onSuccessRegister()
            } else {
                getView()?.onFailRegister(response.Message)
            }
        }, {
            getView()?.onLoadFinished()
            getView()?.onLoadFailed(R.string.try_again)
        }))
    }
}