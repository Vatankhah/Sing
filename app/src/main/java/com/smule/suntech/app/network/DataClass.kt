package com.smule.suntech.app.mvp.model


import kotlin.collections.ArrayList

/**
 * Created by Suntech on 9/3/2017.
 */

interface BaseItem

open class BaseResponse(var Status: String? = "", var Message: String? = "") : BaseItem

data class LoginResponse( var Result:Login? = null) : BaseResponse("" ,"")

data class Login(var Id: String? = "",
                 var Username: String? = "",
                 var CellPhoneNumber: String? = "",
                 var FirstName: String? = "",
                 var LastName: String? = "",
                 var PictureToken: String? = "")

data class RegisterResponce(var Status: String? = "")//todo set other property

data class MusicListResponce(var Result: MusicData, var Message: String? = "", var Status: String?=null)

data class MusicData(var Data: ArrayList<Datum>,
                     var ServerDateTime: String? = "",
                     var PageNumber: String? = "",
                     var PageSize: String? = "",
                     var From: String? = "",
                     var To: String? = "",
                     var TotalCount: String? = "",
                     var HasData: String? = "")

 data class Datum(
        var Id: String?,
        var GenreId: String?,
        var AdminId: String?,
        var Name: String?,
        var PictureToken: String?,
        var AudioToken: String?,
        var Lyric: String?,
        var Date: String?,
        var PersianDate: String?,
        var RelatedItems: RelatedItems?)


open class RelatedItems