package com.smule.suntech.app.domain.interactor

import com.smule.suntech.app.network.repository.Repository
import com.smule.suntech.app.domain.interactor.base.BaseUsecase
import com.smule.suntech.app.domain.models.RegisterModel
import com.smule.suntech.app.domain.models.RegisterResponse
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Suntech on 9/5/2017.
 */

class GetRegisterUseCase @Inject constructor(val mRepository: Repository) : BaseUsecase<RegisterResponse, RegisterModel> {

    var mPhoneNumber: String? = ""

    override fun execute(mRegisterModel: RegisterModel): Observable<RegisterResponse> {
        return mRepository.getRegisterResponse(mRegisterModel).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }
}