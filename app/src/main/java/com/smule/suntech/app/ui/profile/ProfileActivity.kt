package com.smule.suntech.app.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.hamrah.sun.sunpayment.ui.base.BaseActivity
import com.smule.suntech.app.R
import com.smule.suntech.app.domain.models.ProfileData
import com.smule.suntech.app.injector.component.ActivityComponent
import com.smule.suntech.app.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_profile.*
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import java.net.URL






/**
 * Created by S.Vatankhah on 9/26/2017.
 */
class ProfileActivity : BaseActivity<ProfilePresenter, ProfileView>() , ProfileView {
    override fun injectDependencies(component: ActivityComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //val username: String = PreferenceManager.getDefaultSharedPreferences(this).getString("username", "")
        setContentView(R.layout.activity_profile)

        btnSaveProfile.setOnClickListener {
            val mFirstName: String = txtFullName.text.toString();
            val mLastName: String = txtLastName.text.toString();
            val mUserName: String = txtUsername.text.toString();
            if (mUserName.isNullOrEmpty()) {
                txtUsername.requestFocus()
                txtUsername.error = getString(R.string.field_required)
            } else if (mFirstName.isNullOrEmpty()) {
                txtFullName.requestFocus()
                txtFullName.error = getString(R.string.field_required)
            } else if (mLastName.isNullOrEmpty()) {
                txtLastName.requestFocus()
                txtLastName.error = getString(R.string.field_required)
            } else {


                val url = URL("https://sing.blob.core.windows.net/images/87173c84-e5bc-40d9-be99-3079d7d4af1d.png")
                val file = File(url.getPath())
                val body = RequestBody.create(MediaType.parse("images/*"), file)
                val userName = RequestBody.create(MediaType.parse("text/plain"), "Asghar")
                val firtName = RequestBody.create(MediaType.parse("text/plain"), "Asghar")
                val lastName = RequestBody.create(MediaType.parse("text/plain"), "Asghariiiii")

                val map = HashMap<String,RequestBody>()
                map.put("firstName", firtName)
                map.put("lastName", lastName)
                map.put("username", userName)

                mPresenter.saveProfile(ProfileData(userName, firtName, lastName, body))
            }
        }
    }


    override fun onSuccessSaveProfile() {
        intent = Intent(this@ProfileActivity, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onErrorSaveProfile(mString: String?) {

    }

    override fun onLoadStarted() {
        profileProgressBar.visibility = View.VISIBLE
        btnSaveProfile.setText("")
    }

    override fun onLoadFinished() {

    }

    override fun onLoadFailed(msg: Int) {

    }

    override fun onLoadFailed(msg: String) {

    }

}