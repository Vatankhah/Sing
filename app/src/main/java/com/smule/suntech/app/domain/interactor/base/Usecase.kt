package com.smule.suntech.app.domain.interactor.base

import com.smule.suntech.app.domain.executer.PostExecutionThread
import com.smule.suntech.app.domain.executer.UseCaseExecutor
import rx.Scheduler

/**
 * Created by Suntech on 9/18/2017.
 */
public abstract class Usecase<Q,R>//(useCaseExecutor: UseCaseExecutor, postExecutionThread: PostExecutionThread)
{

    private val useCaseExecutor: UseCaseExecutor
    private val postExecutionThread: PostExecutionThread

    constructor(useCaseExecutor: UseCaseExecutor, postExecutionThread: PostExecutionThread) {
        this.useCaseExecutor = useCaseExecutor
        this.postExecutionThread = postExecutionThread
    }


    /**
     * Executes use case. It should/can call {@link #interact(Object)} to get response value.
     */
    abstract fun execute( param: R) : Q
    /**
     * A hook for interacting with given parameter(request value) and returning a response value for each concrete implementation.
     * <p>
     * It should be called inside {@link #execute(Object)}.
     *</p>
     * @param param The request value.
     * @return Returns the response value.
     */
    abstract fun interact(param:R) :Q

    public fun getUseCaseExecutor(): Scheduler
    {
        return useCaseExecutor.getScheduler();
    }

    public fun getPostExecutionThread() : Scheduler
    {
        return postExecutionThread.getScheduler();
    }

}