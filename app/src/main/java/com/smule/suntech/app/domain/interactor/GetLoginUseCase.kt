package com.smule.suntech.app.domain.interactor

import com.smule.suntech.app.domain.interactor.base.BaseUsecase
import com.smule.suntech.app.domain.models.LoginModel
import com.smule.suntech.app.domain.models.LoginResponse
import com.smule.suntech.app.network.repository.Repository
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject


/**
 * Created by Suntech on 9/4/2017.
 */
class GetLoginUseCase @Inject constructor(val mRepository: Repository) : BaseUsecase<LoginResponse, LoginModel> {

    override fun execute(mLoginModel: LoginModel): Observable<LoginResponse> {
        return mRepository.getLoginResponse(mLoginModel).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}