package com.smule.suntech.app.network.repository


import com.smule.suntech.app.domain.models.*

import rx.Observable

/**
 * Created by Suntech on 9/4/2017.
 */
interface Repository {
    fun getPreparedObservable(unPreparedObservable: Observable<*>, clazz: Class<*>, cacheObservable: Boolean, useCache: Boolean): Observable<*>

    fun clearCache()

    // sing api interface

    fun getLoginResponse(mLoginModel: LoginModel): Observable<LoginResponse>

    fun getRegisterResponse(mRegisterModel: RegisterModel): Observable<RegisterResponse>

    fun getMusicListResponse():Observable<MusicListResponce>

    fun getLogout() :Observable<BaseResponse>

    fun getProfile() :Observable<ProfileResponse>

    fun saveProfile(mProfileData: ProfileData): Observable<BaseResponse>
}