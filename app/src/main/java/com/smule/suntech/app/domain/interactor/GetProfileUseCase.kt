package com.smule.suntech.app.domain.interactor

import com.smule.suntech.app.domain.interactor.base.BaseUsecase
import com.smule.suntech.app.domain.models.BaseResponse
import com.smule.suntech.app.domain.models.ProfileData
import com.smule.suntech.app.network.repository.Repository
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by S.Vtankhah on 9/4/2017.
 */
class GetProfileUseCase @Inject constructor(val mRepository: Repository) : BaseUsecase<BaseResponse, ProfileData> {

    override fun execute(mProfileData: ProfileData): Observable<BaseResponse> {
       var mResponse : Observable<BaseResponse> = mRepository.saveProfile(mProfileData).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

        return mResponse ;
    }
}