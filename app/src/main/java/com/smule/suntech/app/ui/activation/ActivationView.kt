package com.smule.suntech.app.ui.activation

import com.smule.suntech.app.ui.base.BaseView

/**
 * Created by Suntech on 9/4/2017.
 */
interface ActivationView : BaseView {
    //todo --> zahra :insert method
    fun onSuccessLogin()

    fun onLoginError(msg: String?)
}