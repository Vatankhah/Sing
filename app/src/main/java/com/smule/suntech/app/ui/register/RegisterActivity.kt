package com.smule.suntech.app.ui.register

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.widget.AppCompatButton
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.TranslateAnimation
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.hamrah.sun.sunpayment.ui.base.BaseActivity
import com.smule.suntech.app.R
import com.smule.suntech.app.domain.models.RegisterModel
import com.smule.suntech.app.injector.component.ActivityComponent
import com.smule.suntech.app.ui.activation.ActivationActivity
import kotlinx.android.synthetic.main.activity_register.*

/**
 * Created by S.Vatankhah on 9/19/2017.
 */

 class RegisterActivity : BaseActivity<RegisterPresenter , RegisterView>(), RegisterView {

    override fun injectDependencies(component: ActivityComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

//        imgRegLeftCloud.startAnimation(AnimationUtils.loadAnimation(this, R.anim.move_left_to_right))
//        imgRegRightCloud.startAnimation(AnimationUtils.loadAnimation(this, R.anim.move_left_to_right))
        setImageLoop()

        var mRegType : String
        if(intent!!.hasExtra("type")) {
            mRegType = intent!!.getStringExtra("type")
            if (mRegType.equals("mobile")) {
                containerMobile.visibility = View.VISIBLE
                containerRegister.visibility = View.GONE
            }
        }
        onClicks();
    }

    private fun setImageLoop() {
        // Need a thread to get the real size or the parent
        // container, after the UI is displayed
        imgRegRightCloud.post(Runnable {
            var mfloat : Float =containerReg.getWidth().toFloat()
            val outAnim = TranslateAnimation(0f, mfloat, 0f, 0f)
            // move from 0 (START) to width (PARENT_SIZE)
            outAnim.interpolator = LinearInterpolator()
            outAnim.repeatMode = Animation.INFINITE // repeat the animation
            outAnim.repeatCount = Animation.INFINITE
            outAnim.duration = 20000

            val inAnim = TranslateAnimation(mfloat , 0f, 0f, 0f)
            // move from out width (-PARENT_SIZE) to 0 (START)
            inAnim.interpolator = LinearInterpolator()
            inAnim.repeatMode = Animation.INFINITE
            inAnim.repeatCount = Animation.INFINITE
            inAnim.duration = 20000 // same duration as the first

            imgRegRightCloud.startAnimation(outAnim) // start first anim
            imgRegLeftCloud.startAnimation(inAnim) // start second anim
        })
    }

    private fun onClicks()
    {

        btnMobileRegister.setOnClickListener{
            containerMobile.visibility = View.VISIBLE
            containerRegister.visibility = View.GONE
        }

        btnMobileSend.setOnClickListener {
            val mPhoneNumber: String = txtMobileNo.text.toString()
            if (mPhoneNumber.isNullOrEmpty()) {
                regMobileNo.requestFocus()
                regMobileNo.error = getString(R.string.field_required)
            } else {
                mPresenter.register(RegisterModel(mPhoneNumber))
            }
        }
    }

    override fun onLoadStarted() {
    regProgressBar.visibility = View.VISIBLE
        btnMobileSend.setText("")
    }


    override fun onSuccessRegister() {

        val mPhoneNumber: String = txtMobileNo.text.toString()

        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString("username", mPhoneNumber)
                .apply()
        Toast.makeText(this, getString(R.string.success_register), Toast.LENGTH_SHORT).show()
        var intent = Intent(this@RegisterActivity, ActivationActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onLoadFinished() {
        mProgressDialog.dismiss()
    }

    override fun onLoadFailed(msg: Int) {
        mProgressDialog.dismiss()
    }

    override fun onFailRegister(msg : String?) {
        MaterialDialog.Builder(this)
                .title(msg.toString())
                .positiveText(getString(R.string.ok))
                .onPositive { materialDialog, _ -> materialDialog.dismiss() }
                .show()
    }

    override fun onLoadFailed(msg: String) {
        mProgressDialog.dismiss()
    }


}


