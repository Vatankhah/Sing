package com.smule.suntech.app.ui.musicList

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import com.smule.suntech.app.R
import android.support.v7.widget.RecyclerView
import com.hamrah.sun.sunpayment.ui.base.BaseFragment
import com.smule.suntech.app.domain.models.MusicListResponce
import com.smule.suntech.app.injector.component.FragmentComponent
import kotlinx.android.synthetic.main.fragment_music_list.*


/**
 * Created by S.Vatankhah on 9/6/2017.
 */

class MusicListFragment : BaseFragment<MusicListPresenter, MusicListItemView>(), MusicListItemView {
    override val layout: Int
        get() = R.layout.fragment_music_list

    override fun injectDependencies(component: FragmentComponent) {
        component.inject(this)
    }

    override fun onCreateCompleted() {
        mSwipeRefreshLayout.setOnRefreshListener {
                mPresenter.onViewAttached(this)
        }
    }

    private val mRecyclerView: RecyclerView by lazy {
        music_item_rv
    }
    private val mSwipeRefreshLayout: SwipeRefreshLayout by lazy {
        music_list_sw
    }

    override fun onLoadStarted() {
        mSwipeRefreshLayout.isRefreshing = true
    }

    override fun onLoadFinished() {
        mSwipeRefreshLayout.isRefreshing = false
    }

    override fun bindMusicListResponse(mMusicListResponse: MusicListResponce) {
        //todo zahra set 'let' method to check null pointer
        val mLayoutManager = GridLayoutManager(activity,3)
        mRecyclerView.layoutManager = mLayoutManager
        val mAdapter = MusicListAdapter(mMusicListResponse.Result.Data)
        mRecyclerView.adapter = mAdapter
    }

    override fun onLoadFailed(msg: Int) {

    }

    override fun onLoadFailed(msg: String) {

    }

    override fun unSuccessGetItem() {

    }
}


