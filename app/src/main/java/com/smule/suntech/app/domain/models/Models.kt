package com.smule.suntech.app.domain.models

/**
 * Created by S.vatankhah on 9/19/2017.
 */

open class BaseModel()

open class RegisterModel(val mPhoneNumber: String, val mChannel: Int = 0)
open class LoginModel(val cellPhoneNumber: String, val verificationCode: String, val mChannel: Int = 0)


data class Datum(
        var Id: String?,
        var GenreId: String?,
        var AdminId: String?,
        var Name: String?,
        var PictureToken: String?,
        var AudioToken: String?,
        var Lyric: String?,
        var Date: String?,
        var PersianDate: String?,
        var RelatedItems: RelatedItems?)


open class RelatedItems